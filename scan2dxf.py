# Imports
import time
import RPi.GPIO as GPIO
import os
import sys
import re
import shutil

# Définition des pins
pinBouton = 17 
pinOkLed = 27
pinBusyLed = 22
GPIO.setmode(GPIO.BCM)

# Définition des pins en entrée / sortie
GPIO.setup(pinBouton, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(pinOkLed, GPIO.OUT)
GPIO.setup(pinBusyLed, GPIO.OUT)

i=1

GPIO.output(pinOkLed,GPIO.LOW)
# On purge le répertoire /home/pi/DXF
os.system ('sudo rm -rf /home/pi/DXF/*') 

# On cherche un scanner
print ("Détection du scanner")
liste_scanner = os.popen("sudo scanimage -L").read()

# Si aucun scanner n'est détecté le programme est stoppé
if  liste_scanner.startswith("\n") == True :
    print ("Aucun scanner n'a pu être détecté, vérifiez le branchement ou la compatibilité du matériel et relancez le programme")
    sys.exit()
else :   
    modele = (re.findall(r"\`(.+?)\'", liste_scanner))[0]
    print ("Scanner détecté : " +modele)
    print("Appuyez sur le bouton pour lancer le scan")
    GPIO.output(pinOkLed,GPIO.HIGH)
    # Notre boucle infinie
    while True:
        
        etat = GPIO.input(pinBouton)
        
        # On vérifie si le bouton a été appuyé : etat==0 => bouton appuyé
        if (etat == 0) :
            GPIO.output(pinOkLed,GPIO.LOW)
            GPIO.output(pinBusyLed,GPIO.HIGH)
            # On définit les noms des différents fichiers qui seront utilisés
            fichier = "Scan_" +str(i)
            fichier_jpg = fichier +".jpg" #résultat du scan
            fichier_pbm = fichier +".pbm" #résultat du traitemetn bitmap
            fichier_dxf = fichier +".dxf" #résultat de la vectorisation
            
            # On lance la numérisation
            print("Lancement du " +fichier +", numérisation en cours")
            
            # Pour modele Canon Lide 120
            os.system ('sudo scanimage -d ' +modele +" --resolution 300 --brightness 30 --contrast 30 > " +fichier_jpg)
            
            print (fichier +" terminé, début du traitement de l\'image")
            
            # On lance le traitement bitmap pour éliminer les imperfections du scan
            os.system ('mkbitmap -n -s 2 -b 6 -t 0.7 ' +fichier_jpg)
            print("Traitement de l'image terminé, traçage de la forme à découper")
            
            # On effectue la vectorisation avec potrace
            os.system ('potrace ' +fichier_pbm +" -A 180 -x 0.0833 -b dxf")
            print ("Traçage de la forme terminé")
            
            # On déplace le fichier DXF vers un dossier partagé
            shutil.move(fichier_dxf, "/home/pi/DXF")
            print ("Fichier " +fichier_dxf +" disponible pour la découpe")
            i += 1
            print()
            
            # Tout est terminé, on attend pour un autre scan
            print("Appuyez sur le bouton pour lancer un nouveau scan")
            GPIO.output(pinBusyLed,GPIO.LOW)
            GPIO.output(pinOkLed,GPIO.HIGH)
        else :
            pass
        
        # Petite pause pour eviter la surchauffe du cpu
        time.sleep(0.3)
